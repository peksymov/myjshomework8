const eventFocus = document.getElementById('inputId');
let InputValue = document.getElementById('inputId');

eventFocus.onfocus = function() {
    eventFocus.classList.add('focused');  // Делаем рамку зеленой
}

eventFocus.onblur = function () {      // При снятии фокуса
    if ((InputValue.value < 0) || !Number(InputValue.value)) {  // условие
        const mistake = document.createElement('span');  // создаем строку
        mistake.innerHTML = 'Введите ЧИСЛО больше 0';
        eventFocus.after(mistake);                      // вставляем его после input
        eventFocus.classList.toggle('change-color-mistake');  // меняем цвет инпута при "ошибке"
        const brake = document.createElement('br');  // создаем br
        eventFocus.after(brake);
    } else if (InputValue.value >= 0) {
        let createSpan = document.createElement('span'); // создаем span
        createSpan.innerHTML = `Текущая цена $ ${InputValue.value}`;
        let label = document.getElementById('label');
        label.before(createSpan);
        eventFocus.classList.add('change-color'); //добавляем класс по смене цввета в input
        createSpan.classList.add('span-decor'); // стили для самого span
        const createButton = document.createElement('button'); // создаем кнопку сброса
        createSpan.insertBefore(createButton,createSpan[0]);
        createButton.innerHTML = 'x';
        createButton.classList.add('button-decor');
        const brake = document.createElement("br");
        label.before(brake);
        createButton.onclick = function () {
            InputValue.value = "";
            createSpan.remove();
            eventFocus.classList.toggle('change-color'); //убираем класс по смене цввета в input если он уже есть
            brake.remove();
        }
    }
}



